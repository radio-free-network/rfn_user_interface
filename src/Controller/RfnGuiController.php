<?php declare(strict_types = 1);

namespace Drupal\rfn_user_interface\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Rfn user interface routes.
 */
final class RfnGuiController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
